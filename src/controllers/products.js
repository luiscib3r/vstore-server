const Product = require('../models/Product')

const ctrl = {}

const fs = require('fs')

ctrl.index_get = async (req, res) => {
    const products = await Product.find().sort({date: -1})

    res.json({products})
}

ctrl.index_post = async (req, res) => {
    var fileName = new Date().getTime() + '.' + req.body.format

    fs.writeFile(
        'public/images/' + fileName,
        Buffer.from(req.body.image, "base64"),
        err => { if(err) console.log(err) }
    )

    const newProduct = new Product({
        name: req.body.name,
        description: req.body.description,
        imageURL: 'http://10.42.0.128:3000/images/' + fileName,
        public_id: fileName,
    })

    await newProduct.save()

    res.json({
        status: 200
    })
}

module.exports = ctrl