const { Router } = require('express')
const router = Router()

const products = require('../controllers/products')

router.get('/', (req, res) => {
    res.send('VStore API 0.1')
})

router.get('/products', products.index_get)
router.post('/products', products.index_post)

module.exports = router